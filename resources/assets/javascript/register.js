$(document).ready(function ()
{
    validateOnChange('#first_name', {
        title: 'First Name',
        required: true
    }, 'First Name is valid', 'Please enter a valid first Name');

    validateOnChange('#last_name', {
        title: 'Last Name',
        required: true
    }, 'Last name is valid', 'Please enter a valid last name');

    validateOnChange('#phone', {
        title: 'Phone Number',
        numeric: true,
        range: {
            min: 11,
            max: 11
        }
    }, 'Phone Number is valid', 'Please enter a valid phone number');

    validateOnChange('#email', {
        title: 'Email',
        required: true
    }, 'Email is valid', 'Please enter a valid email address');


    validateOnChange('#company', {
        title: 'Company',
        required: true
    }, 'Company is valid', 'Please enter a valid Company');

    validateOnChange('#purpose', {
        title: 'Purpose',
        required: true
    }, 'Purpose is valid', 'Please enter a valid purpose');

    validateOnChange('#description', {
        title: 'Description',
        required: false
    }, 'Description is valid', 'Please enter a valid description');

    validateOnChange('#employee_id', {
        title: 'Employee',
        numeric: true,
        required: true
    }, 'Employee is Valid', 'Employee not in system');
});

const output                = document.querySelector('#output');
const userfile              = document.querySelector("#userfile");
const captureVideoButton    = document.querySelector('.capture-button');
const screenshotButton      = document.querySelector('#screenshot-button');
const cancelButton          = document.querySelector('#cancel-button');
// const img = document.querySelector('#screenshot img');
const video                 = document.querySelector('.videostream');
const canvas                = document.createElement('canvas');


const videoConstraints      = {
    facingMode: 'user'
  };
  const constraints         = {
    video: videoConstraints,
    audio: false
  };
function handleError(error) {
  console.error('Error: ', error);
}


cancelButton.onclick        = captureVideoButton.onclick = function() {
    // video.setAttribute("style", "display: block;");
    navigator.mediaDevices
    .getUserMedia(constraints)
    .then(stream => {
      video.srcObject       = stream;
    })
    .catch(error => {
      console.error(error);
    });
};

screenshotButton.onclick    = video.onclick = function() {
  canvas.width              = video.videoWidth;
  canvas.height             = video.videoHeight;
  canvas.getContext('2d').drawImage(video, 0, 0);
  // Other browsers will fall back to image/png

  // video.pause();
  // video.src = "";
  // stream.stop();
  output.setAttribute("style", "display: block;");
  video.setAttribute("style", "display: none;");
  const dataURL             = canvas.toDataURL('image/webp');
  userfile.value            = dataURL;
  output.src                = dataURL;
};



function handleSuccess(stream) {
  // screenshotButton.disabled = false;
  video.srcObject = stream;
  // return navigator.mediaDevices.enumerateDevices();
}



    // const output            = $('#output');
    // const userfile          = $("#userfile");
    // const captureVideoButton= $('.capture-button');
    // const screenshotButton  = $('#screenshot-button');
    // const cancelButton      = $('#cancel-button');
    // const video             = $('.videostream');
    // const canvas            = $('<canvas></canvas>');
    // const videoConstraints  = {
    //     facingMode: 'user'
    // };
    // const constraints         = {
    //     video: videoConstraints,
    //     audio: false
    // };



    // cancelButton.onclick        = captureVideoButton.onclick = function() {
    //     navigator.mediaDevices
    //     .getUserMedia(constraints)
    //     .then(stream => {
    //       video.srcObject       = stream;
    //     })
    //     .catch(error => {
    //       console.error(error);
    //     });
    // };

    // screenshotButton.onclick    = video.onclick = function() {
    //   canvas.width  = video.videoWidth;
    //   canvas.height = video.videoHeight;
    //   canvas.getContext('2d').drawImage(video, 0, 0);
    //   // Other browsers will fall back to image/png

    //   // video.pause();
    //   // video.src = "";
    //   // stream.stop();
    //   output.setAttribute("style", "display: block;");
    //   video.setAttribute("style", "display: none;");
    //   const dataURL     = canvas.toDataURL('image/webp');
    //   userfile.value    = dataURL;
    //   output.src        = dataURL;
    // };

        $("#form_search").submit(function (event)
    {
        // Stop Form From Submitting Normally
        event.preventDefault();

        var $form           = $(this),
            first_name      = $("#first_name"),
            last_name       = $("#last_name"),
            email           = $("#email"),
            phone           = $("#phone"),
            employee_id     = $("#employee_id"),
            purpose         = $("#purpose:selected"),
            description     = $("#description"),
            company         = $("#company"),
            image           = $("#userfile"),
            csrfToken       = $form.children("input[name='csrfToken']"),
            message         = '',
            submitBtn        = $('#submitBtn'),
            processing      = '<i class="fa fa-spinner fa-pulse"></i> Checking in...',
            defaultButton   = '<i class="fa fa-sign-in"></i> Checkin',
            route           = $("#linkRoute").text();

        if (typeof (purpose.val()) == "undefined")
            purpose = 1;
        else
            purpose = 0;
        toastr.clear();
        var error = false;
        $('#form_search .form-control').each(function ()
        {
            var message = $(this).parent().children(".errorMessage").html();
            if ($(this).val() == "")
            {
                error = true;
                toastr.options.timeOut += 3000;
                toastr.error(message);
            }
        });
        if (error) {
            setTimeout(function ()
            {
                window.location = route;
            }, 2000);
            return false;
        }
        disable(submitBtn); submitBtn.html(processing);
        $('#form_search .form-control').each(function ()
        {
            disable($(this));
        });

        $.ajax
        ({
            url: route+"register",
            type: 'post',
            data:
            {
                csrfToken:      csrfToken.val(),
                first_name:     first_name.val(),
                last_name:      last_name.val(),
                email:          email.val(),
                phone:          phone.val(),
                employee_id:    employee_id.val(),
                company:        company.val(),
                purpose:        purpose,
                description:    description.val(),
                img:            image.val(),
                actionType:     'ajax'
            },
            success: function(data, status)
            {
                var dataObj = JSON.parse(data);
                enable(submitBtn);
                submitBtn.html(defaultButton);
                $('#form_search .form-control').each(function ()
                {
                    enable($(this));
                });
                if (dataObj.messageTypeValue == 'danger')
                {
                    // toastr.error(dataObj.message);
                    swal({
                        title: 'Failed!',
                        text: 'Check In Failed.',
                        type: 'error',
                        timer: 2000
                    });
                    setTimeout(function ()
                    {
                        window.location = dataObj.location;
                    }, 2000);
                    return false;
                }
                $('#form_search .form-control').each(function ()
                {
                    $(this).val('');
                });
                toastr.success(dataObj.message);
                swal({
                    title: 'Checked In!',
                    text: 'Checked In Successfully.',
                    type: 'success',
                    timer: 2000
                });
                setTimeout(function ()
                {
                    window.location = dataObj.location;
                }, 2000);
            },
            error: function(xhr, desc, err)
            {
                $('#form_search .form-control').each(function ()
                {
                    enable($(this)); $(this).val('');
                });
                toastr.error('An Error Occurred Try Again.');
                swal({
                    title: 'Failed!',
                    text: 'Check In Failed.',
                    type: 'error',
                    timer: 2000
                });
                setTimeout(function ()
                {
                    window.location = route;
                }, 2000);
                return false;
            }
        });
        event.stopPropagation();
        return false;
    });

function handleSuccess(stream) {
  // screenshotButton.disabled = false;
  video.srcObject   = stream;
  // return navigator.mediaDevices.enumerateDevices();
}

function handleError(error) {
  console.error('Error: ', error);
}