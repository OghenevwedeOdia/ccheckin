	<div class="text-center copyright">
	    <?= Detail::appName(); ?> &copy; Copyright <?= date('Y')."&nbsp;"; ?><br /><?= Detail::company(); ?> 
	    Powered By <a href="http://transitit.com" target="_blank"><?= Detail::company(); ?></a>
	</div>

<!-- <div class="left-sidebar-backdrop"></div> -->
<!-- jQuery core JavaScript -->
<script type="text/javascript" src="<?= __file(JS.'jquery.min.js'); ?>"></script>

<script type="text/javascript" src="<?= __file(JS.'jquery.storageapi.min.js'); ?>"></script>
<!-- Bootstrap core JavaScript -->
<script type="text/javascript" src="<?= __file(JS.'bootstrap.min.js'); ?>"></script>
<!-- Bootstrap tooltips -->
<script type="text/javascript" src="<?= __file(JS.'popper.min.js'); ?>"></script>
<!-- MDB core JavaScript -->
<script type="text/javascript" src="<?= __file(JS.'mdb.min.js'); ?>"></script>
<!-- <script src='http://cdnjs.cloudflare.com/ajax/libs/jquery/2.1.3/jquery.min.js'></script> -->
<script type="text/javascript" src="<?= __file(JS.'jquery.easing.min.js'); ?>"></script>
<script type="text/javascript" src="<?= __file(JS.'jquery.validate.js'); ?>"></script>
<script type="text/javascript" src="<?= __file(JS.'index.js'); ?>"></script>
<!-- <script type="text/javascript" src="<?php //echo base_url(); ?>js/camera.js"></script> -->

<script type="text/javascript" src="<?= __file(JS.'approve.min.js'); ?>"></script>
<script type="text/javascript" src="<?= __file(JS.'jquery-ui.js'); ?>"></script>
<script type="text/javascript" src="<?= __file(JS.'sweetalert2.min.js'); ?>"></script>
<script type="text/javascript" src="<?= __file(JS.'select2.full.min.js'); ?>"></script>
<script type="text/javascript" src="<?= __file(JS.'toastr.min.js'); ?>"></script>
<!-- <script type="text/javascript" src="<?= __file(JS.'sweet-alert.js'); ?>"></script> -->
<script type="text/javascript" src="<?= __file(JS.'toastr.js'); ?>"></script>
<script type="text/javascript" src="<?= __file(JS.'functions.js'); ?>"></script>