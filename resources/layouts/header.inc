<!DOCTYPE html>
<html lang="en">
    <head>
        <title><?= $title; ?></title>
        <meta name="description" content="<?= $description; ?>" />
        <meta name="author" content="<?= Detail::author(); ?>" />
        <link rel="icon" href="<?= __file(IMAGE.'favicon.ico'); ?>" />
        <meta charset="utf-8" />
        <meta http-equiv="X-UA-Compatible" content="IE=edge" />
        <!-- Tell the browser to be responsive to screen width -->
        <meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no" />
        <!-- Fonts -->
        <link rel="stylesheet" type="text/css" href="https://fonts.googleapis.com/css?family=Roboto:300,400,500,700" />
        <link rel="stylesheet" type="text/css" href="https://fonts.googleapis.com/icon?family=Material+Icons" />
        <link rel="stylesheet" type="text/css" href="https://cdnjs.cloudflare.com/ajax/libs/flag-icon-css/1.0.0/css/flag-icon.min.css" />
        <link rel="stylesheet" type="text/css" href="https://cdnjs.cloudflare.com/ajax/libs/weather-icons/2.0.9/css/weather-icons.min.css" />
        <link rel="stylesheet" type="text/css" href="https://cdnjs.cloudflare.com/ajax/libs/weather-icons/2.0.9/css/weather-icons-wind.min.css" />
        <link href="https://cdnjs.cloudflare.com/ajax/libs/meyer-reset/2.0/reset.min.css" rel="stylesheet"/>
       <!-- Material Design Bootstrap -->
        <link rel="stylesheet" href="<?= __file(CSS.'mdb.min.css'); ?>" />
        <link rel="stylesheet" href="<?= __file(CSS.'style.min.css'); ?>" />
        <link rel="stylesheet" href="<?= __file(CSS.'myStyle.css'); ?>" />
        <!-- Font Awesome -->
        <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/font-awesome/4.7.0/css/font-awesome.min.css" />
        <!-- Bootstrap core CSS -->
        <link rel="stylesheet" href="<?= __file(CSS.'bootstrap.min.css'); ?>" />
        <link rel="stylesheet" href="<?= __file(CSS.'jquery-ui.css'); ?>"/>
        <link rel="stylesheet" href="<?= __file(CSS.'sweetalert.css'); ?>" >
        <link rel="stylesheet" href="<?= __file(CSS.'select2.min.css'); ?>" >
        <link rel="stylesheet" href="<?= __file(CSS.'toastr.css'); ?>" >

    </head>
	<div style="display: none;">
		<span id="imageRoute"><?= __file(IMAGE); ?></span>
		<span id="linkRoute"><?= __url("./"); ?></span>
		<span id="currentRoute"><?= ".".\Route::$route; ?></span>
	</div>