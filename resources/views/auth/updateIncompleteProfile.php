<?php layout("header", get_defined_vars()); ?>
<body id="pages-sign-up" data-layout="empty-view" data-controller="pages" data-view="sign-up">
    <div id="fakeloader"></div>
    <div class="form-container">
        <form id="updateForm" class="unlock-account" action="<?= __url('./update-incomplete-profile'); ?>" method="POST">
            <?php $secure->csrfTokenTag(); ?>
            <img src="<?= $user->getProfilePicture(); ?>" class="img-circle img-fluid" alt="<?= $user->getEmptyProperty('fullName'); ?>" />
            <span class="name-badge"><?= $user->getEmptyProperty('fullName'); ?></span>
            <h3 class="header-text">UPDATE PROFILE: <span><?= $user->getEmptyProperty('username'); ?></span></h3>
            <div class="form-group warning has-warning">
                <label for="fullName">Full Name:</label>
                <input type="text" id="fullName" name="fullName" value="<?= $user->fullName; ?>" class="form-control" />
                <span class="bmd-help">Please enter your full name</span>
                <span class="errorMessage">Please enter your full name</span>
            </div>
            <label>Sex: </label>
            <label class="radio-inline radio-warning">
                <input type="radio" class="sex" name="sex" value="m" /> Male
            </label>
            <label class="radio-inline radio-warning">
                <input type="radio" class="sex" name="sex" value="f" /> Female
            </label>
            <div class="form-group warning has-warning">
                <label for="email">Email:</label>
                <input type="email" id="email" name="email" value="<?= $user->email; ?>" class="form-control" />
                <span class="bmd-help">Please enter your email</span>
                <span class="errorMessage">Please enter your email</span>
            </div>
            <div class="form-group warning has-warning">
                <label for="newPassword">New Password:</label>
                <input type="password" id="newPassword" name="newPassword" class="form-control" />
                <span class="bmd-help">Please enter your new password</span>
                <span class="errorMessage">Please enter your new password</span>
            </div>
            <button id="updateBtn" class="btn btn-raised btn-lg btn-warning btn-block updateBtn" type="submit">
                <i class="fa fa-user-plus"></i> Update Profile
            </button>
            <p class="sign-up-link text-center"><a href="<?= __url('./'); ?>">Go to Dashboard</a></p>
        </form>
    </div>
    <!-- Footer -->
    <?php layout("footer", get_defined_vars()); ?>
    <script src="<?= __file(JS.'auth/updateIncompleteProfile.js'); ?>"></script>
</body>
</html>