<?php layout("header", get_defined_vars()); ?>
<body id="pages-sign-up" data-layout="empty-view" data-controller="pages" data-view="sign-up">
    <div id="fakeloader"></div>
    <div class="form-container">
        <form id="updateForm" class="unlock-account" action="<?= __url('./update-password'); ?>" method="POST">
            <?php $secure->csrfTokenTag(); ?>
            <img src="<?= $user->getProfilePicture(); ?>" class="img-circle img-fluid" alt="<?= $user->getEmptyProperty('fullName'); ?>" />
            <span class="name-badge"><?= $user->getEmptyProperty('fullName'); ?></span>
            <h3 class="header-text">UPDATE PASSWORD</h3>
            <div class="form-group warning has-warning">
                <label for="newPassword">New Password:</label>
                <input type="password" id="newPassword" name="newPassword" class="form-control" />
            </div>
            <div class="form-group warning has-warning">
                <label for="cnewPassword">Confirm Password:</label>
                <input type="password" id="cnewPassword" name="cnewPassword" class="form-control" />
            </div>
            <button id="updateBtn" class="btn btn-raised btn-lg btn-warning btn-block updateBtn" type="submit">
                <i class="fa fa-user-plus"></i> Update Password
            </button>
            <p class="sign-up-link text-center"><a href="<?= __url('./'); ?>">Go to Dashboard</a></p>
            <p class="sign-up-link text-center"><a href="<?= __url('./logout'); ?>">Logout</a></p>
        </form>
    </div>
    <!-- Footer -->
    <?php layout("footer", get_defined_vars()); ?>
</body>
</html>