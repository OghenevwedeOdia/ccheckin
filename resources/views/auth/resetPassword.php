<?php layout("header", get_defined_vars()); ?>
<body id="pages-unlock-account" data-layout="empty-view" data-controller="pages" data-view="unlock-account">
    <div id="fakeloader"></div>
    <div class="form-container">
        <form id="resetPasswordForm" class="unlock-account" action="<?= __url('./reset-password'); ?>" method="POST">
            <?php $secure->csrfTokenTag(); ?>
            <input type="hidden" id="id" name="id" value="<?= $user->id; ?>" />
            <input type="hidden" id="encodedUser" name="encodedUser" value="<?= $encodedUser; ?>" />
            <img src="<?= $user->getProfilePicture(); ?>" class="img-circle img-fluid" alt="<?= $user->getEmptyProperty('fullName'); ?>" />
            <span class="name-badge"><?= $user->getEmptyProperty('fullName'); ?></span>
            <h3 class="header-text">RESET PASSWORD</h3>
            <div class="form-group warning has-warning">
                <label for="npassword">New Password:</label>
                <input type="password" id="npassword" name="npassword" class="form-control" />
                <span class="bmd-help">Please enter your new password</span>
                <span class="errorMessage">Please enter your new password</span>
            </div>
            <div class="form-group warning has-warning">
                <label for="cnpassword">Confirm New Password:</label>
                <input type="password" id="cnpassword" name="cnpassword" class="form-control" />
                <span class="bmd-help">Please confirm your new password</span>
                <span class="errorMessage">Please confirm your new password</span>
            </div>
            <button id="resetPasswordBtn" class="btn btn-raised btn-lg btn-warning btn-block" type="submit">
                <i class="fa fa-refresh"></i> Reset Password
            </button>
            <?php if (!$session->isUserLoggedIn()): ?>
                <p class="sign-up-link">Existing user? <a href="<?= __url('./login'); ?>">Login here</a></p>
            <?php else: ?>
                <p class="sign-up-link text-center"><a href="<?= __url('./users', TRUE); ?>">Users</a></p>
            <?php endif; ?>
        </form>
    </div>
    <!-- Footer -->
    <?php layout("footer", get_defined_vars()); ?>
    <script src="<?= __file(JS.'auth/resetPassword.js'); ?>"></script>
</body>
</html>