<?php layout("header", get_defined_vars()); ?>
<body id="pages-sign-up" data-layout="empty-view" data-controller="pages" data-view="sign-up">
    <div id="fakeloader"></div>
    <div class="form-container">
        <form id="registerForm" class="sign-up" action="<?= __url('./register'); ?>" method="POST">
            <?php $secure->csrfTokenTag(); ?>
            <h3 class="header-text">REGISTER</h3>
            <div class="form-group warning has-warning">
                <label for="accountType">Account Type:</label>
                <select class="form-control" id="accountType" name="accountType">
                    <option value="">Select account type:</option>
                    <?php $user = User::findById($session->getID()); ?>
                    <?= Account::generateAccountSelect($user->accountType); ?>
                </select>
                <span class="bmd-help">Please select your account type</span>
                <span class="errorMessage">Please select your account type</span>
            </div>
            <div class="form-group warning has-warning">
                <label for="fullName">Full Name:</label>
                <input type="text" id="fullName" name="fullName" class="form-control" />
                <span class="bmd-help">Please enter your full name</span>
                <span class="errorMessage">Please enter your full name</span>
            </div>
            <label>Sex: </label>
            <label class="radio-inline radio-warning">
                <input type="radio" class="sex" name="sex" value="m" /> Male
            </label>
            <label class="radio-inline radio-warning">
                <input type="radio" class="sex" name="sex" value="f" /> Female
            </label>
            <div class="form-group warning has-warning">
                <label for="email">Email:</label>
                <input type="email" id="email" name="email" class="form-control" />
                <span class="bmd-help">Please enter your email</span>
                <span class="errorMessage">Please enter your email</span>
            </div>
            <div class="form-group warning has-warning">
                <label for="username">Username:</label>
                <input type="text" id="username" name="username" class="form-control" />
                <span class="bmd-help">Please enter your username</span>
                <span class="errorMessage">Please enter your username</span>
            </div>
            <div class="form-group warning has-warning">
                <label for="password">Password:</label>
                <input type="password" id="password" name="password" class="form-control" />
                <span class="bmd-help">Please enter your password</span>
                <span class="errorMessage">Please enter your password</span>
            </div>
            <button id="registerBtn" class="btn btn-raised btn-lg btn-warning btn-block" type="submit">
                <i class="fa fa-user-plus"></i> Register
            </button>
            <p class="sign-up-link text-center"><a href="<?= __url('./'); ?>">Go to Dashboard</a></p>
        </form>
    </div>
    <!-- Footer -->
    <?php layout("footer", get_defined_vars()); ?>
    <!-- <script src="<?= __file(JS.'auth/register.js'); ?>"></script> -->
</body>
</html>