<?php layout("header", get_defined_vars()); ?>
<body id="pages-sign-in" data-layout="empty-view" data-controller="pages" data-view="sign-in">
    <div id="fakeloader"></div>
    <div class="form-container">
        <form id="loginForm" class="sign-in" action="<?= __url('./login'); ?>" method="POST">
            <?php $secure->csrfTokenTag(); ?>
            <h3 class="header-text">LOGIN</h3>
            <div class="form-group warning has-warning">
                <label for="userDetail">Username / Email:</label>
                <input type="text" id="userDetail" name="userDetail" class="form-control" />
                <span class="bmd-help">Please enter your username/email</span>
                <span class="errorMessage">Please enter your username/email</span>
            </div>
            <div class="form-group warning has-warning">
                <label for="password">Password:</label>
                <input type="password" id="password" name="password" class="form-control" />
                <span class="bmd-help">Please enter your password</span>
                <span class="errorMessage">Please enter your password</span>
            </div>
            <div class="checkbox checkbox-warning">
                <label><input type="checkbox" id="rememberMe" name="rememberMe" value="yes" />Remember me?</label>
            </div>
            <button id="loginBtn" class="btn btn-raised btn-lg btn-warning btn-block" type="submit">
                <i class="fa fa-sign-in"></i> Login
            </button>
            <p class="sign-up-link">
                <a class="float-right" href="<?= __url('./forgot-password'); ?>">Forgot Password?</a>
            </p>
        </form>
    </div>
    <!-- Footer -->
    <?php layout("footer", get_defined_vars()); ?>
    <script src="<?= __file(JS.'auth/login.js'); ?>"></script>
</body>
</html>