<?php layout("header", get_defined_vars()); ?>
<body id="pages-forgot-password" data-layout="empty-view" data-controller="pages" data-view="forgot-password">
    <div id="fakeloader"></div>
    <div class="form-container">
        <form id="forgotPasswordForm" class="forgot-password" action="<?= __url('./forgot-password'); ?>" method="POST">
            <?php $secure->csrfTokenTag(); ?>
            <h3 class="header-text">FORGOT PASSWORD</h3>
            <div class="alert alert-info" role="alert">
                <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                    <span aria-hidden="true">×</span>
                    <span class="sr-only">Close</span>
                </button>
                <strong id="message"><i class="fa fa-info"></i></strong> Please enter your email address to receive password reset link.
            </div>
            <div class="form-group warning has-warning">
                <label for="email">Email:</label>
                <input type="email" id="email" name="email" class="form-control" />
                <span class="bmd-help">Please enter your email</span>
                <span class="errorMessage">Please enter your email</span>
            </div>
            <button id="forgotPasswordBtn" class="btn btn-raised btn-lg btn-warning btn-block" type="submit">
                <i class="fa fa-link"></i> Get Link
            </button>
            <p class="sign-up-link">Existing user? <a href="<?= __url('./login'); ?>">Login here</a></p>
        </form>
    </div>
    <!-- Footer -->
    <?php layout("footer", get_defined_vars()); ?>
    <script src="<?= __file(JS.'auth/forgotPassword.js'); ?>"></script>
</body>
</html>