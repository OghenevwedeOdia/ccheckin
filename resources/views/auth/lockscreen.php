<?php layout("header", get_defined_vars()); ?>
<body id="pages-unlock-account" data-layout="empty-view" data-controller="pages" data-view="unlock-account">
    <div id="fakeloader"></div>
    <div class="form-container">
        <form id="unlockAccountForm" class="unlock-account" action="<?= __url('./lockscreen'); ?>" method="POST">
            <?php $secure->csrfTokenTag(); ?>
            <img src="<?= $user->getProfilePicture(); ?>" class="img-circle img-fluid" alt="<?= $user->getEmptyProperty('fullName'); ?>" />
            <span class="name-badge"><?= $user->getEmptyProperty('fullName'); ?></span>
            <h3 class="header-text">UNLOCK ACCOUNT</h3>
            <div class="alert alert-danger" role="alert">
                <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                    <span aria-hidden="true">×</span>
                    <span class="sr-only">Close</span>
                </button>
                <strong id="message"><i class="fa fa-warning"></i></strong> No activity within <?= $timeElapsed; ?>, please enter password to unlock account.
            </div>
            <div class="form-group warning has-warning">
                <label for="password">Password:</label>
                <input type="password" id="password" name="password" class="form-control" />
                <span class="bmd-help">Please enter your password</span>
                <span class="errorMessage">Please enter your password</span>
            </div>
            <button id="unlockAccountBtn" class="btn btn-raised btn-lg btn-warning btn-block" type="submit">
                <i class="fa fa-lock"></i> Unlock Account
            </button>
            <p class="sign-up-link">
                <a class="float-left" href="<?= __url('./logout'); ?>">Logout</a>
                <a class="float-right" href="<?= __url('./forgot-password'); ?>">Forgot Password?</a>
            </p>
        </form>
    </div>
    <!-- Footer -->
    <?php layout("footer", get_defined_vars()); ?>
    <script src="<?= __file(JS.'auth/lockscreen.js'); ?>"></script>
</body>
</html>