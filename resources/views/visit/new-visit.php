<?php layout("header", get_defined_vars()); ?>
	<body>
		<div class="wrapper">
			<form class="steps" method="post" id="form_search" enctype="multipart/form-data" action="">
				<?php $secure->csrfTokenTag(); ?>
	        	<fieldset style="opacity: 0.9;">
	            	<h2 class="fs-title">Welcome to Techbarn:</h2>
	            	<h3 class="fs-subtitle">Kindly fill out your details below to sign in</h3>
	            	<div class="row">
	                	<div class="col-md-6">
	                 		<div class="md-form" >
								<input type="text" id="first_name" name="first_name" class="form-control" />
	                			<label for="first_name">Firstname </label>
	                			<span class="errorMessage">Please enter your First name</span>
	            			</div>
	                	</div>
	                	<div class="col-md-6">
	                    	<div class="md-form">
	                			<input type="text" id="last_name" name="last_name" class="form-control" />
	                			<label for="last_name">Lastname</label>
	                			<span class="errorMessage">Please enter your Last name</span>
	            			</div>
	                	</div>
	            	</div>
			       	<div class="md-form">
	                	<input type="text" id="email" name="email" class="form-control" />
	                	<label for="email">Email Address</label>
	                	<span class="errorMessage">Please enter your email address</span>
	            	</div>
	            	<div class="md-form">
	                	<input type="text" id="phone" name="phone" class="form-control" onchange="allowNumbers();" />
	                	<label for="phone">Telephone</label>
	                	<span class="errorMessage">Please enter your Telephone Number</span>
	            	</div>
	            	<div class="md-form">
	                	<input type="text" id="company" name="company" class="form-control" />
	                	<label for="company">Company </label>
	                	<span class="errorMessage">Please enter your Company name</span>
	            	</div>
	            	<button type="button" data-page="2" name="next" class=" hide btn next action-button" value="Next">Next</button>
	        	</fieldset>
	            <!-- Employee Info -->
	        	<fieldset>
	        		<p>Please input company (Techbarn) employee name you would like to see.</p>
	        		<div class="row">
	        			<div class="col-md-12">
	        				<div class="md-form">

		        				<select class="form-control select2" id="employee_id" name="employee_id">
			                        <option value="">Employee:</option>
			                        <?= AppHelper::generateObjSelect(\Employee::findAll(), ['id', 'first_name', 'last_name']); ?>
			                    </select>
			                    <span class="errorMessage">Please choose an Employee you would like to see</span>
		        			</div>
	        			</div>
	        		</div>
		            <div class="form-group">
		              	<div class="container">
		                	<div class="row">
		                		<div class="col-md-5" style="padding-left: 0;" >
		                			<label for="purpose">Purpose of Visit</label>
		              			</div>
		            			<div>
		                    		<input type="radio" name="purpose" id="radio1" class="radio" value="1" checked/>
		                    		<label for="radio1"name="purpose" value="Official">Official</label>
		                    	</div>&nbsp;&nbsp;
		                    	<div>
		                    		<input type="radio" name="purpose" id="radio2" class="radio" value="0"/>
		                    		<label for="radio2"name="purpose" value="Official">Unofficial</label>
		                    	</div>
		               		</div>
		            	</div>
		     		</div>
		     		<div class="md-form">
		     			<p>Comments:</p>
		                <textarea class="form-control rounded-0" id="description" placeholder="160 characters..." name="sender_purpose" rows="5"></textarea>
		                <span class="errorMessage">Please give an Overview</span>
		     		</div>
		            
		        		<button type="button" data-page="5" name="previous" class=" hide btn btn-primary previous action-button" value="Previous">Previous</button>
	        			<button type="button" data-page="3" name="next" class="capture-button hide btn next action-button " value="Next">Next</button>
	        	</fieldset>
	        	<!-- Camera FIELD SET -->  
	        	<fieldset style="opacity: 0;">
		            <h2 class="fs-title">Take a Picture</h2>
		            <h3 class="fs-subtitle">Let us know how you look like :)?</h3>
		            <div class="container">
		             	<div class="row justify-content-center">
	             			<div class="col-md-8">
			                    <div class="container-fluid">
			                        <video class="videostream img-thumbnail" autoplay playsinline></video>
			                        <img id="output" class="img-thumbnail" style="display: none;" />
			                        <input class="form-control" type="hidden" id="userfile" value="">
			                        <span class="errorMessage">Please Take a Picture</span>
			                    </div>
			                </div>
		             	</div>
			            <div class="row justify-content-center">
		                    <button type="button" id="screenshot-button" class="col-md-4 btn btn-primary btn-sm">
		                        <i class="fa fa-camera fa-2x" aria-hidden="true"></i> Capture
		                    </button>
		                    <button type="button" id="cancel-button" class="col-md-4 btn btn-primary btn-sm">
		                        <i class="fa fa-close fa-2x" aria-hidden="true"></i> Cancel
		                    </button>
		            	</div>
	                </div>
		            <!-- End Final Calc -->
		            <button type="button" data-page="5" name="previous" class=" hide btn previous action-button" value="Previous">Previous</button>
	        		<button type="button" data-page="3" name="next" class=" hide btn next action-button" value="Next">Next</button>
	            	<div class="explanation btn btn-small modal-trigger" data-modal-id="modal-3">Need Help?</div>
	        	</fieldset>
			    <!-- NDA -->
			    <fieldset>
			        <div class="md-form" style="margin-top:0;">
	                   <h4 style="color:w">Techbarn NDA</h4>
	                   <p>Welcome to Techbarn!</p>
	                   <p style="text-align:justify"> <?php echo nl2br("We hope you enjoy your visit, While you're here, you may see or hear information that is confidential to Techbarn, its members or business partners.
	                   You agree not to photograph or record any information that you come across during your visit. If you or your company has a nondisclosure agreement (NDA) with Techbarn, by clicking the check box below you acknowledge that information you see or hear during your visit is confidential under that NDA, and we agree that the terms of the NDA will prevail over this agreement.
	                   
	                   For those of you whose visit is not covered under an existing NDA, the information that you see or hear during your visit is the confidential information of Techbarn and you agree that you will not take,use, discuss, share or disclose this information without Techbarn's written permission. We agree that you don't need our permission if you can show that something is already generally publicly known through no fault of yours or breach by others.
	                   
	                   Thanks for your cooperation. Have a great day.");?></p>
	                   
	        		</div>
	        		<button type="button" data-page="5" name="previous" class="hide btn previous action-button" value="Previous">Previous</button>
	            	<button id="submitBtn" class="btn large action-button next" type="submit" style="width:200px;" value="Submit">I Agree</button>
	        	</fieldset>   
	    	</form>
	    	<div class="push"></div>
	    </div>
    	<?php layout("footer", get_defined_vars()); ?>
    	<script src="<?= __file(JS.'register.js'); ?>"></script>
    	
	</body>
</html>