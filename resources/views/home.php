<?php layout("header", get_defined_vars()); ?>
	<body>
		<div class="wrapper">
			<section class="vertical-center">
				<div class="container-fluid">
					<center>
						<img src="<?= __file(IMAGE.'company.png') ?>" class="img-fluid"/>
						<br/>
						<a class="btn btn-blue btn-hover btn-lg mt-1" href="<?= __url('new-visit') ?>">SIGN IN</a>
					</center>
				</div>
			</section>
			<div class="push"></div>
		</div>

		<?php layout("footer", get_defined_vars()); ?>
	</body>
</html>
