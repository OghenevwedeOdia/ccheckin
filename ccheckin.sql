-- phpMyAdmin SQL Dump
-- version 4.6.4
-- https://www.phpmyadmin.net/
--
-- Host: 127.0.0.1
-- Generation Time: Sep 05, 2018 at 09:26 PM
-- Server version: 5.7.14
-- PHP Version: 7.0.10

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `ccheckin`
--

-- --------------------------------------------------------

--
-- Table structure for table `employees`
--

CREATE TABLE `employees` (
  `id` int(11) NOT NULL,
  `first_name` varchar(255) NOT NULL,
  `last_name` varchar(255) NOT NULL,
  `email` varchar(255) NOT NULL,
  `phone` text NOT NULL,
  `created` datetime NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `employees`
--

INSERT INTO `employees` (`id`, `first_name`, `last_name`, `email`, `phone`, `created`) VALUES
(1, 'shade', 'shade', 'shade@gmail.com', '08124040519', '2018-08-25 10:25:21'),
(2, 'bola', 'bola', 'bola@gmail.com', '08124040519', '2018-08-25 07:43:28');

-- --------------------------------------------------------

--
-- Table structure for table `receptionist`
--

CREATE TABLE `receptionist` (
  `id` int(11) NOT NULL,
  `first_name` varchar(255) NOT NULL,
  `last_name` varchar(255) NOT NULL,
  `email` varchar(255) NOT NULL,
  `phone` text NOT NULL,
  `password` varchar(255) NOT NULL,
  `created` datetime NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `visitors`
--

CREATE TABLE `visitors` (
  `id` int(11) NOT NULL,
  `first_name` varchar(255) NOT NULL,
  `last_name` varchar(255) NOT NULL,
  `email` varchar(255) NOT NULL,
  `phone` text NOT NULL,
  `company` varchar(255) NOT NULL,
  `image` varchar(255) NOT NULL,
  `created` datetime NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `visitors`
--

INSERT INTO `visitors` (`id`, `first_name`, `last_name`, `email`, `phone`, `company`, `image`, `created`) VALUES
(1, 'odia', 'oghenevwede', 'odiaoghenevwede@gmail.com', '08124040519', 'Cage', 'img20180825145745.png', '2018-08-25 14:57:45'),
(4, 'odia', 'oghenevwede', 'king@king.com', '08124040519', 'Cage', 'img20180825155707.png', '2018-08-25 15:57:07');

-- --------------------------------------------------------

--
-- Table structure for table `visits`
--

CREATE TABLE `visits` (
  `id` int(11) NOT NULL,
  `employee_id` int(11) NOT NULL,
  `visitor_id` int(11) NOT NULL,
  `description` varchar(255) NOT NULL,
  `purpose` int(1) NOT NULL,
  `created` datetime NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `visits`
--

INSERT INTO `visits` (`id`, `employee_id`, `visitor_id`, `description`, `purpose`, `created`) VALUES
(1, 1, 1, '3err', 1, '2018-08-25 15:21:23'),
(2, 2, 1, 'ddd', 1, '2018-08-25 15:23:24'),
(3, 2, 1, 'k.s', 1, '2018-08-25 15:26:10'),
(4, 2, 1, 'dddd', 1, '2018-08-25 15:32:49'),
(5, 1, 1, 'dsd', 1, '2018-08-25 15:35:07'),
(6, 2, 1, 'hdh', 1, '2018-08-25 15:38:15'),
(7, 2, 1, 'dddshj', 1, '2018-08-25 15:48:25'),
(8, 1, 1, 'dnk', 1, '2018-08-25 15:58:03'),
(9, 1, 1, '', 1, '2018-09-01 15:51:03'),
(10, 1, 1, '', 1, '2018-09-01 15:51:23'),
(11, 1, 1, '', 1, '2018-09-01 15:52:36'),
(12, 1, 1, '', 1, '2018-09-01 15:52:59'),
(13, 1, 1, '', 1, '2018-09-01 15:59:51'),
(14, 1, 1, '', 1, '2018-09-01 16:00:23'),
(15, 1, 1, '', 1, '2018-09-01 16:00:35'),
(16, 1, 1, '', 1, '2018-09-01 16:06:57'),
(17, 1, 1, '', 1, '2018-09-01 16:08:34'),
(18, 1, 1, '', 1, '2018-09-01 16:10:20'),
(19, 1, 1, '', 1, '2018-09-01 16:11:35'),
(20, 1, 1, '', 1, '2018-09-01 16:13:19');

--
-- Indexes for dumped tables
--

--
-- Indexes for table `employees`
--
ALTER TABLE `employees`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `receptionist`
--
ALTER TABLE `receptionist`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `visitors`
--
ALTER TABLE `visitors`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `visits`
--
ALTER TABLE `visits`
  ADD PRIMARY KEY (`id`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `employees`
--
ALTER TABLE `employees`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;
--
-- AUTO_INCREMENT for table `receptionist`
--
ALTER TABLE `receptionist`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `visitors`
--
ALTER TABLE `visitors`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=5;
--
-- AUTO_INCREMENT for table `visits`
--
ALTER TABLE `visits`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=21;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
