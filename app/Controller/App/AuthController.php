<?php
	namespace App;

	use Blaze\{Auth\Auth, Encryption\Encrypt, File\FileImage};
    use Blaze\Http\{Mail, Session};
    use Blaze\Validation\{Validator as Validate, FormValidator as FV};

	/**
	* App AuthController Class
	*/
	class AuthController
	{
		use \UserHelperTrait;
	}