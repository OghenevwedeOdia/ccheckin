<?php
	namespace App;

	use Blaze\{Auth\Auth, Encryption\Encrypt};
    use Blaze\Http\{Mail, Session};
    use Blaze\Validation\{Validator as Validate};

	/**
	* App Controller Class
	*/
	class Controller
	{
		use \UserHelperTrait;

		/**
		* Handles the homePage route.
		* @return void
		*/
		public static function homePage ()
		{
			global $session;

	    	$title          = "Home Page | ".APP_NAME;
		    $description    = "Home Page";
		    $messageObj 	= static::getMessage();
			\View::make('home', get_defined_vars());
		}


		/**
		* Handles the dashboard route.
		* @return void
		*/
		public static function dashboard ()
		{
			global $session;

	        $session->lockPage('./lockscreen');
		    $user = static::getLoggedUser();

            $file 			= "dashboard.".strtolower($user->accountType);
	    	$title          = "Dashboard | ".APP_NAME;
		    $description    = "Dashboard Page";
		    $messageObj 	= static::getMessage();
			\View::make($file, get_defined_vars());
		}
	}