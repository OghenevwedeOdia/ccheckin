<?php
	namespace App;

	use Blaze\{Auth\Auth, Encryption\Encrypt};
    use Blaze\Http\{Mail, Session};
    use Blaze\Validation\{Validator as Validate};

	/**
	* App SetController Class
	*/
	class SetController
	{
		use \UserHelperTrait;
	}