<?php
	namespace User;

	use Blaze\{Auth\Auth, Encryption\Encrypt};
    use Blaze\Http\{Mail, Session};
    use Blaze\Validation\{Validator as Validate};

	/**
	* User Controller Class
	*/
	class Controller
	{
		use \UserHelperTrait;

		/**
		* Handles the registration page route.
		* @return void
		*/
		public static function registerPage ()
		{
			global $session, $secure;

	        $session->lockPage('./lockscreen');

			if ($secure->checkRequestType("POST"))
				AuthController::registerUser();

	    	$title          = "Register User | ".APP_NAME;
		    $description    = "Register User Page";
		    $messageObj 	= static::getMessage();
			\View::make('auth.register', get_defined_vars());
		}


		
		/**
		* Handles the login route.
		* @return void
		*/
		public static function loginPage ()
		{
			global $session, $secure;

		    if ($session->isLoggedIn()) 	redirectTo("./lockscreen");
			if ($session->isUserLoggedIn()) redirectTo('./');

			if ($secure->checkRequestType("POST"))
				AuthController::logUserIn();

		    $title          = "Login | ".APP_NAME;
		    $description    = "Login Page";
		    $messageObj 	= static::getMessage();
			\View::make('auth.login', get_defined_vars());
		}



		/**
		* Log user out of application.
		* @return void
		*/
		public static function logout ()
		{
			global $session, $session;
		    
			if (Session::getSession("IM_SUPER") && Session::getSession("IM_SUPER_USER")):
				$superUser 			= \User::findById(Session::getSession("IM_SUPER_USER"));
				$link 				= "./".strtolower($superUser->accountType)."/users";
				static::$location 	= [$link, $link];
				$session->login($superUser->id);
				Session::setSession("IM_SUPER", 		NULL);
				Session::setSession("IM_SUPER_USER", 	NULL);
				static::authAction("Welcome Back! <b>".$superUser->getEmptyProperty('fullName')."</b> &#x1f60d;", 'success', static::actionType());
			endif;
			if ($session->isLoggedIn()) $session->logout();
		    redirectTo('./login');
		}

	}