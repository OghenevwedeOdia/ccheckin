<?php
	namespace User;

	use Blaze\{Auth\Auth, Encryption\Encrypt, File\FileImage, Logger\Log as FileLog};
    use Blaze\Http\{Mail, Session};
    use Blaze\Validation\{Validator as Validate, FormValidator as FV};


	/**
	* User AuthController Class
	*/
	class AuthController {
		use \UserHelperTrait;

		/**
		* Registers a new user.
		* @return void
		*/
		public static function registerUser ()
		{
			global $session;

	        $actionType 		= static::actionType();
	        static::$location 	= ['./register', './register'];
	        $requiredFields 	= ['accountType', 'fullName', 'sex', 'password'];
	        if (!FV::validatePresence($requiredFields, 'POST'))		        	
		    	static::authAction(FV::getError(), 'danger', $actionType);

	        $accountType   		= $_POST['accountType'];
	        $fullName   		= trim($_POST['fullName']);
	        $sex   				= $_POST['sex'];
	        $email   			= trim($_POST['email']);
	        $username   		= trim($_POST['username']);
	        $password   		= $_POST['password'];
	        $user 				= \User::findById($session->getID());
	        $validAccountTypes 	= \Account::accountTypes($user->accountType);
        	if (!array_key_exists(strtoupper($accountType), $validAccountTypes))
	        	static::authAction("You don't have access to perform this action", 'danger', $actionType);

        	if (empty($email) && empty($username))
	        	static::authAction("Email or username is required", 'danger', $actionType);

	        if (!empty($email) && !Validate::formatMatch($email, Validate::FORMAT_EMAIL))
	        	static::authAction("Invalid Email", 'danger', $actionType);

        	if (strlen($fullName) < 4)
	        	static::authAction("Name length shouldn't be less than 4", 'danger', $actionType);

        	if (!in_array($sex, ['m', 'f']))
	        	static::authAction("Invalid Sex", 'danger', $actionType);

        	if (strlen($password) < 6)
	        	static::authAction("Password length shouldn't be less than 6", 'danger', $actionType);

	        if (!empty($email) && (new \User)->checkUser($email))
		    	static::authAction("Email already exist try another one.", 'danger', $actionType);
	        if (!empty($username) && (new \User)->checkUsername($username))
		    	static::authAction("Username already exist try another one.", 'danger', $actionType);
		    (new \User)->newUser((object) [
		    	'accountType' 	=> $accountType,
		    	'userID' 		=> $user->id,
		    	'fullName' 		=> $fullName,
		    	'sex' 			=> $sex,
		    	'email' 		=> $email,
		    	'username' 		=> $username,
		    	'password' 		=> $password,
		    ]);
	        $link 				= getHost()."/login";
		    $messageHTML 		= (object) [
		        'mailLog' 		=>  "Login Details Notification",
		        'mailTitle' 	=>  "Login Details.",
		        'mailContent' 	=>  "<p>Dear $fullName, you have been successfully registered.</p>".
	                                "<p><b>Email:</b> $email</p>".
	                                "<p><b>Password:</b> $password</p>".
	                                "<p>You can now login <a href='$link'>here</a></p>".
	                                "<p>It's best you change your password to your preffered one.</p>",
		    ];
			static::sendNotificationMail($fullName, $email, $messageHTML);
	    	static::authAction("User registration completed", 'success', $actionType);
		}


		
		/**
		* Log user in
		* @return void
		*/
		public static function logUserIn ()
		{
			global $secure, $session;

	        $actionType 		= static::actionType();
	        static::$location 	= ['./login', './'];
	        if (!FV::validatePresence(['userDetail','password'], 'POST'))
	        	static::authAction("All fields are required", 'danger', $actionType);

	        $userDetail = trim($_POST['userDetail']);	        
	        $password   = $_POST['password'];
	        $rememberMe = $_POST['rememberMe'] ?? "no";

			$foundUser 	= FALSE;
	        $user 		= new \User;
	        if (Validate::formatMatch($userDetail, Validate::FORMAT_EMAIL)):
		        $foundUser = $user->findByColumn('email', $userDetail);
		    else:
		        $foundUser = $user->findByColumn('username', $userDetail);
		    endif;

	        if (!$foundUser)
	        	static::authAction("Invalid <b>'Username'</b>/<b>'Email'</b>", 'danger', $actionType);
	        	
	        $result = $user->validateUser($password, $foundUser);
	        if ($result == TRUE):
		        if ($foundUser->status == "inactive")
		        	static::authAction("Your account is not active, contact the administrator", 'danger', $actionType);
				Session::setSession("REMEMBER_ME", $rememberMe);
				$session->login($foundUser->id);
				$user->updateLastLogin($foundUser->id);
		    	static::authAction("Successfully Logged in", 'success', $actionType);
        	endif;
	    	static::authAction("Invalid <b>'Password'</b> Try Again.", 'danger', $actionType);
		}
	}