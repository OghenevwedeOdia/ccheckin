<?php
	namespace User;

	use Blaze\{Auth\Auth, Encryption\Encrypt};
    use Blaze\Http\{Mail, Session};
    use Blaze\Validation\{Validator as Validate};

	/**
	* User SetController Class
	*/
	class SetController
	{
		use \UserHelperTrait;
	}