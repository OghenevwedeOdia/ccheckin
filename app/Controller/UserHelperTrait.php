<?php
    use Blaze\{Http\Mail as Mailer, Logger\Log as FileLog};

	/**
	* UserHelper Trait
	*/
	trait UserHelperTrait
	{
		/**
		* Store the location to redirect to.
		* @var array
		*/
		public static $location;

		/**
		* Store smilies.
		* \u{1F60D}
		* @var array
		*/
		public static $smiley = [
			'danger' 	=> '&#x1f608;',
			'success' 	=> '&#x1f60d;',
			'warning' 	=> '&#x1f612;',
		];

		/**
		* Get logged in user
		* @param int $userID
		* @return \User
		*/
		protected static function getLoggedUser (int $userID=0) : \User
		{
			global $session;

			$userID 	= !empty($userID) ? $userID : $session->getID();
		    $user 		= \User::findById($userID);
		    if (!$user) redirectTo("./logout");
			return $user;
		}

		/**
		* Performs an action after validation process.
		* @param string $message
		* @param string $messageTypeValue
		* @param string $actionType
		* @param string $data
		* @return void
		*/
		protected static function authAction (string $message, string $messageTypeValue, string $actionType, string $data=NULL)
		{
	        $location 	= ($messageTypeValue == 'danger') ? (static::$location[0] ?? "") : (static::$location[1] ?? "");
	        $result 	= [
	        	'location' 			=> $location,
	        	'message' 			=> $message,
	        	'messageTypeValue' 	=> $messageTypeValue,
	        	'data' 				=> $data,
        	];

	        if ($actionType === 'ajax'):
			    print json_encode((object) $result); exit;
	        endif;

	        $messageName 	= md5("message");
	        $messageType 	= md5("messageType");
	        $message 		= url($message);

	        if ($actionType === 'php'):
	        	if (stripos($location, '?') !== FALSE)
			        redirectTo("$location&{$messageName}={$message}&{$messageType}=$messageTypeValue");
	        	else
			        redirectTo("$location?{$messageName}={$message}&{$messageType}=$messageTypeValue");
	        endif;
		}

		/**
		* Performs an action after validation process.
		* @param array $messageAndType
		* @param array $location
		* @param string $actionType
		* @param string $data
		* @return void
		*/
		public static function authAction_ (array $messageAndType, array $location, string $actionType, string $data=NULL)
		{
			$messageType 		= $messageAndType[0] ?? "";
			$message 			= $messageAndType[1] ?? "";
			$errorLocation 		= $location[0] ?? "";
			$successLocation 	= $location[1] ?? $errorLocation;
	        $location 			= ($messageType == 'danger') ? $errorLocation : $successLocation;
	        $result 			= [
	        	'location' 		=> $location,
	        	'message' 		=> $message,
	        	'messageType' 	=> $messageType,
	        	'data' 			=> $data,
        	];

	        if ($actionType === 'ajax'):
			    print json_encode((object) $result); exit;
	        endif;

	        $urlMessageName = md5("message");
	        $urlMessageType = md5("messageType");
	        $message 		= url($message);

	        if ($actionType === 'php'):
	        	if (stripos($location, '?') !== FALSE)
			        redirectTo("$location&{$urlMessageName}={$message}&{$urlMessageType}=$messageType");
	        	else
			        redirectTo("$location?{$urlMessageName}={$message}&{$urlMessageType}=$messageType");
	        endif;
		}

		/**
		* Validates and return type of action to be taken
		* @param string $method
		* @return string
		*/
		protected static function actionType (string $method="POST") : string
		{
			$method 	= !in_array(strtoupper($method), ['POST', 'GET']) ? $_POST : $GLOBALS["_".strtoupper($method)];
	        $actionType = $method['actionType'] ?? 'php';
			return !in_array($actionType, ['php','ajax']) ? 'php' : $actionType;
		}

		/**
		* Gets GET message and presents in HTML format.
		* @param string $message
		* @param string $messageType
		* @return stdClass
		*/
		protected static function getMessage ($message='', $messageType="") : stdClass
		{
		    $getMessage     = $_GET[md5("message")]         ?? "";
		    $getMessageType = $_GET[md5("messageType")]     ?? "";
		    $message        = !empty($message) 		? $message 		: $getMessage;
		    $messageType 	= !empty($messageType) 	? $messageType 	: $getMessageType;
		    return (object) ['message' => $message, 'messageType' => $messageType];
		}

		/**
		* Checks if a file exist.
		* @param string $file
		* @return bool
		*/
		public static function checkFile ($file='') : bool
		{
			return (is_file($file) && file_exists($file)) ? TRUE : FALSE;
		}

		/**
		* Send Notification Mail.
		* @param string $fullName
		* @param string $email
		* @param \stdClass $filename
		* @param \stdClass $messageHTML
		* @return bool
		*/
		public static function sendNotificationMailWithAttachment (string $fullName, string $email, \stdClass $attachment, \stdClass $messageHTML) : bool
		{
			global $template;
			
		    $SMTPConfig     = Mailer::setSMTP(MAIL_HOST, MAIL_USERNAME, MAIL_PASSWORD, MAIL_SMTPSECURE, MAIL_PORT);
		    $messageBody 	= $template->mailTemplate($messageHTML, TRUE);
		    $emailBody      = Mailer::setBody(strip_tags($messageHTML->mailTitle), minifyHTMLOutput($messageBody));
		    $from       	= Mailer::setEmail(MAIL_EMAIL, \Detail::appName());
	        $to     		= Mailer::setEmail($email, $fullName);
	        $user 			= "$fullName @: $email";
	        Mailer::sendWithAttachment($emailBody, $to, $from, $from, $attachment, $SMTPConfig);
	    	if (!Mailer::$result):
			    (new FileLog("Mail Notification wasn't sent to $user", "ERROR"))->setLogFile("mailLog.txt")->logMessage();
			    (new FileLog("Error Message:- ".Mailer::$errorMessage." User:- $user", "ERROR"))->setLogFile("mailErrorLog.txt")->logMessage();
		    else:
			    (new FileLog("Mail Notification was sent to $user", "SUCCESS"))->setLogFile("mailLog.txt")->logMessage();
		    endif;
		    return Mailer::$result;
		}


		/**
		* Send Notification Mail.
		* @param string $fullName
		* @param string $email
		* @param \stdClass $messageHTML
		* @return bool
		*/
		public static function sendNotificationMail (string $fullName, string $email, \stdClass $messageHTML) : bool
		{
			global $template;
			
		    $SMTPConfig     = Mailer::setSMTP(MAIL_HOST, MAIL_USERNAME, MAIL_PASSWORD, MAIL_SMTPSECURE, MAIL_PORT);
		    $messageBody 	= $template->mailTemplate($messageHTML, TRUE);
		    $emailBody      = Mailer::setBody(strip_tags($messageHTML->mailTitle), minifyHTMLOutput($messageBody));
		    $from       	= Mailer::setEmail(MAIL_EMAIL, \Detail::appName());
	        $to     		= Mailer::setEmail($email, $fullName);
	        $user 			= "$fullName @: $email";
	        Mailer::sendHtmlEmail($emailBody, $to, $from, $from, $SMTPConfig);
	    	if (!Mailer::$result):
			    (new FileLog("Mail Notification wasn't sent to $user", "ERROR"))->setLogFile("mailLog.txt")->logMessage();
			    (new FileLog("Error Message:- ".Mailer::$errorMessage." User:- $user", "ERROR"))->setLogFile("mailErrorLog.txt")->logMessage();
		    else:
			    (new FileLog("Mail Notification was sent to $user", "SUCCESS"))->setLogFile("mailLog.txt")->logMessage();
		    endif;
		    return Mailer::$result;
		}

		/**
		* Validates pagination page
		* @param string $page
		* @return int
		*/
		public static function validatePage (string $page=NULL) : int
		{
			return ($page >= 1 && is_numeric($page)) ? (int) $page : 1;
		}
	}