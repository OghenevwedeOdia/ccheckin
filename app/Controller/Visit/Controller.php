<?php
	namespace Visit;

	use Blaze\{Auth\Auth, Encryption\Encrypt};
    use Blaze\Http\{Mail, Session};
    use Blaze\Validation\{Validator as Validate};

	/**
	* Visit Controller Class
	*/
	class Controller
	{
		use \UserHelperTrait;
		/**
		* Handles the register page route.
		* @return void
		*/
		public static function newVisitPage ()
		{
			global $session, $secure;

			if ($secure->checkRequestType("POST"))
				AuthController::newVisit();

	    	$title          = "New Visit |".APP_NAME;
		    $description    = "New Visit";
		    $messageObj 	= static::getMessage();
			\View::make("visit.new-visit", get_defined_vars());
		}
	}