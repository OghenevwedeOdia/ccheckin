<?php
	namespace Visit;

	use Blaze\{Auth\Auth, Encryption\Encrypt, File\FileImage, Logger\Log as FileLog};
    use Blaze\Http\{Mail, Session};
    use Blaze\Validation\{Validator as Validate, FormValidator as FV};

	/**
	* Visit AuthController Class
	*/
	class AuthController
	{
		use \UserHelperTrait;
		/**
		* Create New Visit.
		* @return void
		*/
		public static function newVisit ()
		{
			global $secure, $session;

	        $actionType 		= static::actionType();
	        static::$location 	= ["./", "./"];
	        $requiredFields 	= ['employee_id','last_name','first_name','phone','email','purpose','img','company'];
	        
	        if (!FV::validatePresence($requiredFields, 'POST'))
	        	static::authAction(FV::getError(), 'success', $actionType);

	        if (!\Employee::checkEmployee('id', $_POST['employee_id']))
		    	static::authAction("Invalid Employee", 'success', $actionType);

	        $employee_id 		= $_POST['employee_id'];
	        $first_name 		= $_POST['first_name'];
	        $last_name 			= $_POST['last_name'];
	        $phone 				= $_POST['phone'];
	        $email 				= $_POST['email'];
	        $purpose 			= $_POST['purpose'];
	        $company 			= $_POST['company'];
	        $img 				= $_POST['img'];
	        // static::authAction($img, 'danger', $actionType);
	        // Non Required Feilds
	        $description 		= $_POST['description'] 	?? "";

	        $img = str_replace('data:image/png;base64,', '', $img);
			$img = str_replace(' ', '+', $img);
			$data = base64_decode($img);
			$file = 'img'.date("YmdHis").'.png';

			if (file_put_contents(UPLOAD_IMG.$file, $data)===FALSE) 
				static::authAction("Unable to Upload Picture", 'success', $actionType);


		    $visitorObject 	= (object) [
		    	'email' 			=> $email,
		    	'first_name' 		=> $first_name,
		    	'last_name' 		=> $last_name,
		    	'phone' 			=> $phone,
				"company" 			=> $company,
		    	'image' 			=> $file
		    ];

		    if (\Visitor::checkVisitor('email', $email)):
		    	$visitor = \Visitor::findByColumn('email', $email); 
		    else:
		    	$visitor =  new \Visitor;
				$visitor = 	$visitor->newVisitor($visitorObject);
			endif;

		    $visitObject 	= (object) [
		    	'employee_id' 		=> $employee_id,
		    	'visitor_id' 		=> $visitor->id,
		    	'description' 		=> $description,
		    	'purpose' 			=> $purpose
		    ];



		    $visit 	  = new \Visit;
			$visit	  = $visit->newVisit($visitObject);
			$filename = COMPANY.'pdf';
		    $fullName = \Visitor::getFullName($last_name, $first_name);
	        if (!empty($email)):
			    $messageHTML 		= (object) [
			        'mailLog' 		=>  "Visitation Notification",
			        'mailTitle' 	=>  "Visitation.",
			        'mailContent' 	=>  "<p>Hello  $first_name; ?></p>".
									        "<p>Thanks for visiting  techbarn ?></p>".
									        "<p>We've attached a copy for the document you signed today when you checked in.</p>".
									        "<p>Please keep it for your records</p>".
									        "<p>Thanks for stopping by!</p>".
									        "<hr>".
									        "<center><p>Powered by</p></center>".
									        "<center><img class='img-thumbnail' src='<?= __file(IMAGE.'company.png') ?>' /><br></center>"
			    ];

			    $time 	= date(' h:i:a');
				$day 	= date("jS ");
				$month 	= date("F");
				$year 	= date("Y");
				$html 	= "We hope you enjoy your visit, While you're here, you may see or hear information that is confidential to techbarn, its members or business partners.
				You agree not to photograph or record any information that you come across during your visit. If you or your company has a nondisclosure agreement (NDA) with techbarn, by clicking the check box below you acknowledge that information you see or hear during your visit is confidential under that NDA, and we agree that the terms of the NDA will prevail over this agreement.
				For those of you whose visit is not covered under an existing NDA, the information that you see or hear during your visit is the confidential information of techbarn and you agree that you will not take,use, discuss, share or disclose this information without techbarn's writeen permission. We agree that you don't need our permission if you can show that something is already generally publicly known through no fault of yours or breach by others.

				Thanks for your cooperation. Have a great day.
				Signed by :".$fullName."
				Today :".$day.'-'.$month.'-'.$year."
				Time:".$time;

				$pdf 	= new \FPDF('p','mm','A4');
				$pdf-> AddPage();

				$pdf-> setDisplayMode ('fullpage');

				$pdf-> setFont ('times','B',20);
				$pdf-> cell(200,30,"techbarn NDA",0,1);

				$pdf-> setFont ('times','B','20');
				$pdf-> write (10,$html);
				$pdf-> output ($filename,'F');

				$attachment 	= (object) [
			    	'fileName'			=> $filename,
			    	'newFileName'		=> 'CHK'.$filename
			    ];


				static::sendNotificationMailWithAttachment($fullName, $email, $attachment, $messageHTML);
		    endif;

		    $employee = \Employee::findByID($employee_id);

		    if (!empty($employee->email)):
			    $messageHTML 		= (object) [
			        'mailLog' 		=>  "Visitation Notification",
			        'mailTitle' 	=>  "Visitation.",
			        'mailContent' 	=>  "<div class'container'>".
									    "<div class='row justify-content-center'>".
						                    "<div class='col-md-6'>".
							                    "<p>Hello $employee->first_name,</p>".
							                    "<p>$first_name $last_name has arrived to see you and waiting in the reception area.</p>".
						                    "</div>".
						                    "<div class='col-md-6'>".
						                    	"<center>  <img alt='guest' src='<?= __file(UPLOAD_IMG.$filename) ?>' class='img-thumbnail' /></center>".
						                 	"</div>".
									    "</div>".
								        "<hr>".
								        "<center><p>Powered- by</p></center>".
								        "<center><img class='img-thumbnail' src='<?= __file(IMAGE.'company.png') ?>' /><br></center>"
			    ];

			static::sendNotificationMail($fullName, $email, $messageHTML);
		    endif;

	    	static::authAction("Checked in Successfully", 'success', $actionType);
		}

	}