<?php

/**
* @routes GENERAL
*/
Route::register('/', function ()
{
	App\Controller::homePage();
}, "Home Page");

Route::register('/dashboard', function ()
{
	App\Controller::dashboard();
}, "Dashboard");

Route::register('/register', function ()
{
	User\Controller::registerPage();
}, "Register");

Route::register('/login', function ()
{
	User\Controller::loginPage();
}, "Login");

Route::register('/logout', function ()
{
    User\Controller::logout();
}, "Logout");



/**
* @routes MISC
*/
Route::register('/infophp', function ()
{
    phpinfo();
}, "PHP Information");

Route::register('/test', function ()
{
	View::make('test');
}, "Test");

Route::register('/coming', function ()
{
    View::make('AppStates.coming');
}, "Coming");

Route::register('/401', function ()
{
    View::make('AppStates.401');
}, "401");

Route::register('/500', function ()
{
    View::make('AppStates.500');
}, "500");


/**
* @routes Visit
*/
Route::register('/new-visit', function ()
{
    Visit\Controller::newVisitPage();
}, "New Visit");


