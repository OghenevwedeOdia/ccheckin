<?php
	use Blaze\{
		Auth\Auth,
		Database\Database,
		Encryption\Encrypt,
		Database\DatabaseObject,
		Validation\Validator as Validate
	};

	/**
	* Visit Model Class
	*/
	class Visit extends DatabaseObject
	{
		/**
		* Database Table Name
		* @var string $tableName
		*/
		protected static $tableName			= 'visits';
		
		/**
		* Database Columns
		* @var array $databaseFields
		*/
		protected static $databaseFields 	= ['id', 'purpose', 'description', 'employee_id', 'visitor_id', 'created'];

		/**
		* Create New Visit.
		* @param \stdClass $visitObject
		* @return bool
		*/
		public function newVisit (\stdClass $visitObject) : bool
		{
			$this->purpose 			= $visitObject->purpose;
			$this->description 		= $visitObject->description;
			$this->employee_id 		= $visitObject->employee_id;
			$this->visitor_id		= $visitObject->visitor_id;
			$this->created 			= date("Y-m-d H:i:s");
			return $this->save() ? TRUE : FALSE;
		}


		/**
		* Check if Visit exist
		* @param string $column
		* @param string $value
		* @return bool
		*/
		public function checkVisit (string $column, string $value) : bool
		{
			static::$foundObject = static::findByColumn($column, $value);
			return (static::$foundObject) ? TRUE : FALSE;
		}

	}
