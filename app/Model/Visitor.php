<?php
	use Blaze\{
		Auth\Auth,
		Database\Database,
		Encryption\Encrypt,
		Database\DatabaseObject,
		Validation\Validator as Validate
	};

	/**
	* Visitor Model Class
	*/
	class Visitor extends DatabaseObject
	{
		/**
		* Database Table Name
		* @var string $tableName
		*/
		protected static $tableName			= 'visitors';
		
		/**
		* Database Columns
		* @var array $databaseFields
		*/
		protected static $databaseFields 	= ['id', 'first_name', 'last_name', 'email', 'phone', 'company', 'image', 'created'];

		/**
		* Create New Visitor.
		* @param \stdClass $visitorObject
		* @return bool
		*/
		public function newVisitor (\stdClass $visitorObject) : bool
		{
			$this->first_name 		= $visitorObject->first_name;
			$this->last_name 		= $visitorObject->last_name;
			$this->company 			= $visitorObject->company;
			$this->phone 			= $visitorObject->phone;
			$this->email 			= $visitorObject->email;
			$this->image 			= $visitorObject->image;
			$this->created 			= date("Y-m-d H:i:s");
			return $this->save() ? TRUE : FALSE;
		}


		/**
		* Check if Visitor exist
		* @param string $column
		* @param string $value
		* @return bool
		*/
		public static function checkVisitor (string $column, string $value) : bool
		{
			static::$foundObject = static::findByColumn($column, $value);
			return (static::$foundObject) ? TRUE : FALSE;
		}

		/**
		* Get method for full name.
		* @param string $title
		* @param string $surname
		* @param string $firstname
		* @param string $middlename
		* @return string
		*/
		public static function getFullName (string $surname=NULL, string $firstname=NULL, string $middlename=NULL) : string
		{
			return trim("{$surname} {$firstname} {$middlename}");
		}

	}
