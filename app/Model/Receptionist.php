<?php
	use Blaze\{
		Auth\Auth,
		Database\Database,
		Encryption\Encrypt,
		Database\DatabaseObject,
		Validation\Validator as Validate
	};

	/**
	* Receptionist Model Class
	*/
	class Receptionist extends DatabaseObject
	{
		/**
		* Database Table Name
		* @var string $tableName
		*/
		protected static $tableName			= 'receptionist';
		
		/**
		* Database Columns
		* @var array $databaseFields
		*/
		protected static $databaseFields 	= ['id', 'first_name', 'last_name', 'email', 'phone', 'created', 'password'];

		/**
		* Create New Receptionist.
		* @param \stdClass $receptionistObject
		* @return bool
		*/
		public function newReceptionist (\stdClass $receptionistObject) : bool
		{
			$this->first_name 		= $receptionistObject->first_name;
			$this->last_name 		= $receptionistObject->last_name;
			$this->email 			= $receptionistObject->email;
			$this->phone 			= $receptionistObject->phone;
			$this->password 		= Encrypt::passwordEncrypt($receptionistObject->password);
			$this->created 			= date("Y-m-d H:i:s");
			return $this->save() ? TRUE : FALSE;
		}

		/**
		* Update Receptionist last login date.
		* @param int $id
		* @return bool
		*/
		public function updateLastLogin (int $id) : bool
		{
			$this->id 			= $id;
			$this->lastLogin 	= date("Y-m-d H:i:s");
			return $this->save() ? TRUE : FALSE;
		}

		/**
		* Change Receptionist Password.
		* @param int $id
		* @param string $newPassword
		* @return bool
		*/
		public function changeReceptionistPassword (int $id, string $newPassword) : bool
		{
			$this->id 			= $id;
			$this->password 	= Encrypt::passwordEncrypt($newPassword);
			$this->lastUpdated 	= date("Y-m-d H:i:s"); 
			return $this->save() ? TRUE : FALSE;
		}

		/**
		* Check if Receptionist exist
		* @param string $column
		* @param string $value
		* @return bool
		*/
		public function checkReceptionist (string $column, string $value) : bool
		{
			static::$foundObject = static::findByColumn($column, $value);
			return (static::$foundObject) ? TRUE : FALSE;
		}

		/**
		* Receptionist login authentication.
		* @param string $password
		* @param \Receptionist $foundObject
		* @return bool
		*/
		public static function validateReceptionist (string $password, \Receptionist $foundObject) : bool
		{
			// Check password
			return (!Encrypt::passwordCheck($password, $foundObject->password)) ? FALSE : TRUE;
		}
	}
