<?php
	use Blaze\{
		Auth\Auth,
		Database\Database,
		Encryption\Encrypt,
		Database\DatabaseObject,
		Validation\Validator as Validate
	};

	/**
	* Employee Model Class
	*/
	class Employee extends DatabaseObject
	{
		/**
		* Database Table Name
		* @var string $tableName
		*/
		protected static $tableName			= 'employees';
		
		/**
		* Database Columns
		* @var array $databaseFields
		*/
		protected static $databaseFields 	= ['id', 'first_name', 'last_name', 'email', 'phone', 'created'];

		/**
		* Create New Employee.
		* @param \stdClass $employeeObject
		* @return bool
		*/
		public function newEmployee (\stdClass $employeeObject) : bool
		{
			$this->first_name 		= $employeeObject->first_name;
			$this->last_name 		= $employeeObject->last_name;
			$this->email 			= $employeeObject->email;
			$this->phone 			= $employeeObject->phone; 
			$this->created 			= date("Y-m-d H:i:s");
			return $this->save() ? TRUE : FALSE;
		}


		/**
		* Check if Employee exist
		* @param string $column
		* @param string $value
		* @return bool
		*/
		public static function checkEmployee (string $column, string $value) : bool
		{
			static::$foundObject = static::findByColumn($column, $value);
			return (static::$foundObject) ? TRUE : FALSE;
		}


	}
