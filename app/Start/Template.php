<?php
	use Blaze\Encryption\Encrypt;
	use User\MLA\PRC\SetController;

	/**
	* Template Class
	*/
	class Template extends Blaze\TemplateEngine\Template
	{
		/**
		* Mail Template for mail notifications.
		* @param \stdClass $messageHTML
		* @return mixed
		*/
		public function mailTemplate (\stdClass $messageHTML, $return=FALSE)
		{
			$this->setFileLocation("mailTemplate.html");

			$footer 	 = Detail::appName()." &copy; Copyright ".date('Y')."<br />".Detail::company();
			$footer 	.= " Powered By <a href='http://transitit.com' target='_blank'>".Detail::company()."</a>";

			$this->set('title',			strip_tags($messageHTML->mailTitle));
			$this->set('link',			getHost());
			$this->set('dateTime',		datetimeToText(date("Y-m-d H:i:s")));
			$this->set('email',			MAIL_USERNAME);
			$this->set('mailTitle',		$messageHTML->mailTitle);
			$this->set('mailContent',	$messageHTML->mailContent);
			$this->set('appName',		Detail::appName());
			$this->set('footer',		$footer);
			return $this->display($return);
		}

		
	}
